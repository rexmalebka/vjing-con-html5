const inputdiv = document.querySelector("#inpt");
const errorlog = document.querySelector("#log");
const patcomm = /\/\/.*/gm;
//const patobj = /^(video|imagen)\(("[^"]*"|'[^']*')\)/gm;

let keyPressed = [];

const cursorid = setInterval(()=>{
	let curselem = document.querySelector("#cursor");
	if (curselem.style.visibility==""){
		curselem.style.visibility="hidden";
	}else{
		curselem.style.visibility="";
	}
	
},300);



document.body.onkeydown = function(e){
	let befc = document.querySelector("#bcursor");
	let aftc = document.querySelector("#acursor");

	keyPressed.push(e.key);
	
	if (e.key.length == 1 && keyPressed.indexOf("Control") == -1 ){
		//if key is a letter

		let befc = document.querySelector("#bcursor");
		befct = befc.textContent;
		befct += e.key;
		befc.textContent = befct;

	}else if(e.key == "Enter" && keyPressed.indexOf("Control") == -1){
		//if key is an Enter
		
		let pos = getcursorpos();
		let nline = document.createElement("div");
		nline.classList.add("line");
		insertAfter(nline,inputdiv.childNodes[pos]);
		
		befc = inputdiv.childNodes[pos + 1].appendChild(befc);
		cursor = inputdiv.childNodes[pos + 1].appendChild(cursor);
		aftc = inputdiv.childNodes[ pos + 1].appendChild(aftc);
		
		
		inputdiv.childNodes[pos].textContent = befc.textContent;
		befc.textContent = "";
		
		inputdiv.childNodes[pos].id = "";
		inputdiv.childNodes[pos + 1].id = "actual";
		
	}else if (e.key == "ArrowLeft"){
		
		befct = befc.textContent;
		aftct = aftc.textContent;
		if( befct != "" ){
			aftct = befct.charAt(befct.length-1)+aftct;
			befct = befct.substr(0,befct.length-1);
			befc.textContent = befct;
			aftc.textContent = aftct;
		}else{

			
			let pos = getcursorpos();
			if(pos > 0){
				let text = inputdiv.childNodes[pos].textContent;
				let ntext = inputdiv.childNodes[pos-1].textContent;

				while(inputdiv.childNodes[pos-1].hasChildNodes()){
					inputdiv.childNodes[pos-1].removeChild(inputdiv.childNodes[pos-1].lastChild);
				}

				inputdiv.childNodes[pos-1].append(befc);
				inputdiv.childNodes[pos-1].append(cursor);
				inputdiv.childNodes[pos-1].append(aftc);
				
				befc.textContent = ntext;
				aftc.textContent = "";
				inputdiv.childNodes[pos].textContent = text;
				inputdiv.childNodes[pos - 1].id = "actual";
			}

		}
	
	}else if (e.key == "ArrowRight"){
		
		let befct = befc.textContent;
		let aftct = aftc.textContent;
		if( aftct != "" ){
			befct += aftct[0];
			aftct = aftct.substr(1);
			befc.textContent = befct;
			aftc.textContent = aftct;
		}else{

			
			let pos = getcursorpos();
			if(pos < inputdiv.childNodes.length - 1){
				let text = inputdiv.childNodes[pos].textContent;
				let ntext = inputdiv.childNodes[pos+1].textContent;

				while(inputdiv.childNodes[pos+1].hasChildNodes()){
					inputdiv.childNodes[pos+1].removeChild(inputdiv.childNodes[pos+1].lastChild);
				}

				inputdiv.childNodes[pos+1].append(befc);
				inputdiv.childNodes[pos+1].append(cursor);
				inputdiv.childNodes[pos+1].append(aftc);
				
				befc.textContent = "";
				aftc.textContent = ntext;

				inputdiv.childNodes[pos].textContent = text;
				inputdiv.childNodes[pos + 1].id = "actual";
			}

		}
	
	}else if (e.key == "ArrowUp"){
		let pos = getcursorpos();
		if(pos > 0 ){
			let text = inputdiv.childNodes[pos-1].textContent;
			let befct = befc.textContent;
			let aftct = aftc.textContent;

			while(inputdiv.childNodes[pos-1].hasChildNodes()){
				inputdiv.childNodes[pos-1].removeChild(inputdiv.childNodes[pos-1].lastChild);
			}

			inputdiv.childNodes[pos-1].append(befc);
			inputdiv.childNodes[pos-1].append(cursor);
			inputdiv.childNodes[pos-1].append(aftc);
			
			inputdiv.childNodes[pos].textContent = befct + aftct;
			befc.textContent = text.substr(0, befct.length);
			aftc.textContent = text.substr(befct.length);
			
			inputdiv.childNodes[pos].id = "";
			inputdiv.childNodes[pos - 1].id = "actual";
		}
	}else if (e.key == "ArrowDown"){
		let pos = getcursorpos();

		if(pos < inputdiv.childNodes.length-1){
			let text = inputdiv.childNodes[pos+1].textContent;
			let befct = befc.textContent;
			let aftct = aftc.textContent;

			
			while(inputdiv.childNodes[pos+1].hasChildNodes()){
				inputdiv.childNodes[pos+1].removeChild(inputdiv.childNodes[pos+1].lastChild);
			}
			
			inputdiv.childNodes[pos+1].append(befc);
			inputdiv.childNodes[pos+1].append(cursor);
			inputdiv.childNodes[pos+1].append(aftc);

			inputdiv.childNodes[pos].textContent = befct + aftct;
			befc.textContent = text.substr(0, befct.length);
			aftc.textContent = text.substr(befct.length);

			inputdiv.childNodes[pos].id = "";
			inputdiv.childNodes[pos + 1].id = "actual";

		}
	
	}else if(e.key == "Backspace"){

		let befct = befc.textContent;
		if(befct.length > 0){
			befc.textContent = befct.substr(0,befct.length-1);
		}else{
			let pos = getcursorpos();
			if(pos > 0){
				let text = inputdiv.childNodes[pos-1].textContent;

				while(inputdiv.childNodes[pos-1].hasChildNodes()){
					inputdiv.childNodes[pos-1].removeChild(inputdiv.childNodes[pos-1].lastChild);
				}

				inputdiv.childNodes[pos-1].append(befc);
				inputdiv.childNodes[pos-1].append(cursor);
				inputdiv.childNodes[pos-1].append(aftc);

				befc.textContent = text;
				inputdiv.removeChild(inputdiv.childNodes[pos]);

				inputdiv.childNodes[pos - 1].id = "actual";

			}			

		}
		
	}else if(e.key == "Delete"){
		let aftct = aftc.textContent;
		if(aftct.length > 0){
			aftc.textContent = aftct.substr(1);
		}else{
			let pos = getcursorpos();
			if(pos < inputdiv.childNodes.length-1){
				let text = inputdiv.childNodes[pos+1].textContent;

				inputdiv.removeChild(inputdiv.childNodes[pos + 1]);
				aftc.textContent = text;
		
		}
	}

	}else if(e.key == "Home"){
		let befct = befc.textContent;
		befc.textContent = "";
		aftc.textContent = befct + aftc.textContent;

	}else if(e.key == "End"){
		let aftct = aftc.textContent;
		aftc.textContent = "";
		befc.textContent =  befc.textContent + aftct;
	}
	
	if(keyPressed.indexOf("Control") != -1 && keyPressed.indexOf("Enter") != -1){
		console.log("ejecutan2");
		execute();

	}

	process();

};


document.onkeyup = function(e){
	keyPressed = keyPressed.filter((x)=>{return e.key !== x});
};

function getcursorpos(){
	let pos = 0;
	let m = false;
	inputdiv.childNodes.forEach((n,k)=>{

		if( n.childNodes.length == 3 && n.childNodes[1] == cursor){
			pos = k;
			m = true;
		}
	});
	return pos;
}

function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}


function process(){
	/*
	let befc = document.querySelector("#bcursor");
	let aftc = document.querySelector("#acursor");
	console.log("\n**************");
	inputdiv.childNodes.forEach( (n,k) => {
		if(n.id != "actual"){
			let text = n.textContent;
			let m = text.match(patcomm);
			let commentaries = "";
			let ind = text.lenght;

			if(m !=null && m.length == 1  ){
				//detect commentaries

				ind = text.indexOf(m[0]);
				commentaries = text.substr(ind);	

			}
			
			let instructions = text.substr(0,ind).split(";").filter( (k) => {return k != ""});
			console.log(instructions, commentaries);


		}else{
			/*
			let befct = befc.textContent;
			let aftct = aftc.textContent;
			let binstructions = befct.split(";").filter( (k) => {return k != "" } );
			let ainstructions = aftct.split(";").filter( (k) => {return k != "" } );
			console.log(binstructions,"",ainstructions);
		}

	});
			*/
}

function execute(){
	let text = "";
	let lineas = [];
	inputdiv.childNodes.forEach( (n) => {
		if(n.textContent != "" ){
			lineas.push( n.textContent);
		}
	});
	text = lineas.join("\n");
	console.log("'"+text+"'");
	try {
		eval(text); 
	} catch (e) {
		errorlog
		errorlog.childNodes[0].textContent = e.name;
		errorlog.childNodes[1].textContent = e.message;
	}

}
