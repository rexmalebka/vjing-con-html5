const board = document.querySelector("#board");

const Cont = function( src ){

	//all property from this function
	if(Cont.all == undefined) Cont.all = [];

	//insert a  container in the board
	let container = document.createElement( "div" );
	container.classList.add( "container" );
	container = board.appendChild( container );

	//get sum properties like width and height
	let img = document.createElement("img");
	img.src = "images/"+src;

	//returned object
	const obj = {
		src : src,
		container : container,
		img : img,
		_width : "100%",
		_height : "100%",
		last : [],
		out : function(){
			//outputs everything
			
			this.fx.forEach( ( k ) => {	

				//create a tag for nesting
				const content = document.createElement( "div" );
				content.classList.add( "content" );
				content.setAttribute( "fx", k.name );
				content.id = "last" ;
				
				//last child
				const last = this.container.querySelectorAll( "#last" );
				
				//if not present append it, neither nest it
				if(last.length == 0){
					this.container.appendChild( content );
				}else{
					last.forEach( function( t ){
						content.style = t.style;
						t.appendChild( content );
						t.removeAttribute("id");
					});
				}

				//update last 
				this.last = this.container.querySelectorAll( "#last" );
			});

			this.last.forEach(function( l ){

				//add images
				l.append(this.img);
				console.log(this.img);

			}.bind(this));
			
			this.container.querySelectorAll(".content").forEach(function( c, indx ){
				console.log(indx,c,this.fx[indx])
			}.bind(this));
		},
		fx : [ ],
		bands : function( n ){
			this.bands.num = n;
			this.container;
			return this
		},

		blur : function( amount ){

			if( ! isNaN( Number(amount) ) && Number( amount ) >= 0 ){

				//add to the fx list
				this.fx.push( { name : "blur", amount : amount } );

			}
			
			this.blur.apply = function ( value ){
				//this function adds blur to the last

			}.bind(this);


			return this

		}
	};

	
	//all property contains an array of objects made from this.
	Cont.all.push( obj );

	//objects could also be indexed
	Cont[ Cont.all.length - 1 ] = obj;

	return obj

};




