const board = document.querySelector("#board"); 

function loop(t){
	
	vjtest.videosObjects.forEach(function(k){
		if(k.tag.paused || k.tag.currentTime < k._start || k.tag.currentTime > k._end){
			k.tag.src = k.tag.src.split("#")[0]+"#t="+k._start+","+k._end;
			k.tag.playbackRate = k._rate; 
			k.tag.play()
		}

		/*
		if (k.time < k._start ){
			k.tag.pause();
			k.tag.currentTime = k._start;			
			k.tag.play();

		}else if (k.time > k._end ){
			k.tag.pause();
			k.tag.currentTime = k._start;
			k.tag.play();
		}
		*/
	});
	
	requestAnimationFrame(loop);
}

requestAnimationFrame(loop);

vjtest = {
	videosObjects : [],
	imagesObjects : [],
	apply : function(){
		this.videosObjects.forEach(function(k){

		});
	},
	refresh : function (){
		this.videosObjects.forEach(function(k){
			
		});
	},
	refreshid : 0,
	applyid : 0
};

const video = function (src){	
	return {
		src : src,
		loop : true,
		_start : 0,
		_end : 3,
		_rate : 1,
		fx : [],
		tag : false,
		out : function(){


			Object.defineProperty(this,"time",
				{ 
					get : function(){
						return  document.querySelector("video").currentTime;
						
					},
				}
			);

			Object.defineProperty(this, "start",
				{
					get : function(t){
						return this._start;
					},
					set : function(t){
						if(t >= this.tag.duration || t < 0){
							this._start = 0;
						}else{
							this._start = t
						}
					}
				}
			);

			Object.defineProperty(this, "end",
				{
					get : function(){
						return this._end
					},
					set : function(t){
						if(t < this._start || t > this.tag.duration){
							this._end = this.tag.duration;
						}else{
							this._end = t
						}
					}
			}
			);
			Object.defineProperty(this, "rate",
				{
					get : function(){
						return this._rate
					},
					set : function(t){
						this._rate = t;
						this.tag.playbackRate = t;
					}
			}
			);

			vjtest.videosObjects.push(this);
			
			let vid = document.createElement("video");
			vid.src = "videos/"+this.src+"#t=0,1";
			vid.width = "500";
			//vid.style.
			vid.loop = true;
			vid.autoplay = true;
			vid.muted = true;
			vid.classList.add("media");
			board.appendChild(vid);

			this.tag = vid;
			
		},

		flipx : function(){
			let copy = this;
			copy.fx.push({"name":"flipx","value":true});
			return copy
		},

		flipy : function(){
			let copy = this;
			copy.fx.push({"name":"flipy","value":true})
			return copy
		},

		blur : function(px=0){
			let copy = this;
			copy.fx.push({"name":"blur","value":px});
			return copy
		},

		gray : function(perc=0){
			let copy = this;
			copy.fx.push({"name":"gray","value":perc});
			return copy
		},

		saturate : function(perc=0){
			let copy = this;
			copy.fx.push({"name":"saturate","value":perc});
			return copy
		},


	}

};


