# framework de livecoding con videos

quiero hacer un framework para modificar videos en vivo con javascript y HTML5

El plan es que pudiera agregar videos en vivo y poder editarlos usando css básico

Planes para efectos:

* blur
* contrast
* brightness
* rotateX
* rotateY
* alpha
* colormode
* loop

Por el momento existe un bug en los navegadores para manejar video en el DOM y loopearlo, esperaré que en un futuro lo arreglen

También es que estoy más ocupado últimamente, así que no sé cuando podré continuar con este proyecto. :( 
